package signals;

import (
    "context"
    "os"
    "os/signal"
)

func Init(ctx context.Context, cancel context.CancelFunc){
    go func(){
        channel:=make(chan os.Signal, 1);
        signal.Notify(channel, os.Interrupt);
        signalLoop:
        for {
            select{
            case <-ctx.Done():
                break signalLoop;
            case sig:=<-channel:
                if sig == os.Interrupt {
                    cancel();
                    break signalLoop;
                }
            }
        }
    }();
}
