package types;

import(
    "context"
    "encoding/json"
    "flag"
    "fmt"
    "os"
)

const (
    LANG_FILE="public/lang/es.json";
)

//Predefined errors
var(
    LanguageMap=readLanguage();
    ErrBadPassword=fmt.Errorf(LanguageMap["BadCredentials"]);
    ErrWeakPassword=fmt.Errorf(LanguageMap["WeakPassword"]);
    ErrBadRequest=fmt.Errorf(LanguageMap["BadRequest"]);
    ErrServerFailure=fmt.Errorf(LanguageMap["SambaError"]);
)

func readLanguage() map[string]string{
    var mp map[string]string;
    file,err:=os.Open(LANG_FILE);
    if err != nil{
        panic(err);
    }
    defer file.Close();
    parser:=json.NewDecoder(file);
    err=parser.Decode(&mp);
    if err != nil{
        panic(err);
    }
    return mp;
}

type SambaRequest struct{
    Context context.Context; //The context package says we shouldn't include this on structs, but this is a special case
    ResponseChannel chan *Response;
    RequestType int;
    RequestData *APIDataIn;
}

func CreateSambaRequest(ctx context.Context, data *APIDataIn) *SambaRequest{
    return &SambaRequest{
        Context: ctx,
        ResponseChannel: make(chan *Response, 1),
        RequestType: data.Type,
        RequestData: data,
    }
}

type Response struct{
    Ok bool;
    ResponseData string;
    Error error;
    Status int;
}

type Config struct{
    Addr, SambaHost, ClientDir string;
}

func ConfigFromFlags() *Config{
    cnf:=&Config{};
    flag.StringVar(&cnf.Addr, "addr", ":8080", "Listen Address like 0.0.0.0:8080");
    flag.StringVar(&cnf.SambaHost, "host", "localhost", "Samba host to query and change password");
    flag.StringVar(&cnf.ClientDir, "client-dir", "client/public", "Client dir to serve");
    flag.Parse();
    return cnf;
}

type APIDataIn struct{
    Type int;
    Username, Password, NewPassword string;
}

type APIDataOut struct{
    HasError bool;
    Status int;
    Message string;
}
