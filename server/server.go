package server;

import(
    "context"
    "gitlab.com/mrvik/logger"
    "gitlab.com/mrvik/smb-web-client/types"
    "net"
    "net/http"
    "time"
)

const(
    SERVER_CLOSE_TIMEOUT=time.Second*10;
)

var (
    log logger.Logger;
    config *types.Config;
    serveMux *http.ServeMux;
    sambaChannel chan<- *types.SambaRequest;
)

var handlersToRegister=[]func()(string, http.Handler){
    func()(string, http.Handler){
        return "/", http.RedirectHandler("/client/", http.StatusPermanentRedirect);
    },
    func()(string, http.Handler){
        return "/client/", http.StripPrefix("/client/", http.FileServer(http.Dir(config.ClientDir)));
    },
    func()(string, http.Handler){
        return "/api/", &APIHandler{sambaChannel};
    },
}

func Init(ctx context.Context, cnf *types.Config, lgr logger.Logger) (<-chan *types.SambaRequest){
    log=lgr;
    config=cnf;
    serveMux=http.NewServeMux();
    channel:=make(chan *types.SambaRequest, 1);
    sambaChannel=channel;
    go startServer(ctx, channel);
    return channel;
}

func startServer(ctx context.Context, channel chan<- *types.SambaRequest) {
    srv:=&http.Server{
        Addr: config.Addr,
        Handler: serveMux,
        BaseContext: contextProvider(ctx),
    }
    for _,fn:=range(handlersToRegister){
        serveMux.Handle(fn());
    }
    go func(){
        <-ctx.Done();
        closeCtx,cancel:=context.WithTimeout(context.Background(), SERVER_CLOSE_TIMEOUT);
        defer cancel();
        err:=srv.Shutdown(closeCtx);
        if err != nil && err != http.ErrServerClosed {
            log.Errorf("Error while closing server: %s\n", err);
        }
    }();
    log.Infof("Listening on %q\n", config.Addr);
    err:=srv.ListenAndServe();
    if err != nil && err != http.ErrServerClosed{
        panic(err);
    }
}

func contextProvider(ctx context.Context) func(net.Listener) context.Context{
    return func(_ net.Listener) context.Context{
        return ctx;
    }
}
