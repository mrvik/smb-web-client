package server;

import(
    "context"
    "encoding/json"
    "gitlab.com/mrvik/smb-web-client/types"
    "net/http"
    "time"
)

const MAX_REQUEST_TIME=time.Second*20;

//Request types. We start at 1 to avoid uninitialized types
const(
    REQ_TRY_PASSWORD=1;
)

type APIHandler struct{
    channel chan<- *types.SambaRequest;
}

func (api *APIHandler) ServeHTTP(w http.ResponseWriter, r *http.Request){
    if r.Method != http.MethodPost {
        api.badRequest(w);
        return;
    }
    decoder:=json.NewDecoder(r.Body);
    var inData types.APIDataIn;
    err:=decoder.Decode(&inData);
    if err != nil{
        api.badRequest(w);
        return;
    }
    ctx, cancel:=context.WithTimeout(r.Context(), MAX_REQUEST_TIME);
    defer cancel();
    api.passToSamba(ctx, w, &inData);
}

func (api *APIHandler) passToSamba(ctx context.Context, w http.ResponseWriter, data *types.APIDataIn){
    req:=types.CreateSambaRequest(ctx, data);
    api.channel<-req;
    response:=<-req.ResponseChannel;
    if response.Status == 0 && response.Error == types.ErrBadPassword {
        response.Status=http.StatusUnauthorized;
    }
    api.respondAPI(w, response);
}

func (api *APIHandler) respondAPI(w http.ResponseWriter, response *types.Response){
    msg:=response.ResponseData;
    if response.Error != nil{
        msg=response.Error.Error();
    }
    dataOut:=types.APIDataOut{
        HasError: response.Error!=nil,
        Status: response.Status,
        Message: msg,
    }
    if response.Status != 0 && response.Status != http.StatusOK {
        w.WriteHeader(response.Status);
    }
    encoder:=json.NewEncoder(w);
    encoder.Encode(&dataOut);
}

func (api *APIHandler)badRequest(w http.ResponseWriter){
    http.Error(w, "Bad request", http.StatusBadRequest);
}
