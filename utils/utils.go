package utils;

import (
    "strings"
)

func AnyStringIs(match string, stringsToCheck ...string) bool{
    for _,str:=range stringsToCheck{
        if str == match{
            return true;
        }
    }
    return false;
}

func JoinStrings(separator string, joinStrings ...string) string{
    return strings.Join(joinStrings, separator);
}
