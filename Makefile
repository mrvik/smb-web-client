.PHONY: default all build server client install uninstall clean

sinclude Makefile.Header

all: | build

build: | server client

server:
	+go build -v -buildmode=pie ${FLAGS} -o ${PKGNAME} .

client:
	+$(MAKE) -C client/ build

install: | install-server install-client systemd-service

install-server:
	install -Dm755 ${PKGNAME} ${DESTDIR}${PREFIX}/bin/${PKGNAME}

install-client:
	$(MAKE) -C client/ install

systemd-service:
	install -Dm644 init/${PKGNAME}.service ${DESTDIR}${PREFIX}/lib/systemd/system/${PKGNAME}.service

uninstall: | uninstall-server uninstall-client

uninstall-server:
	rm -f ${DESTDIR}${PREFIX}/bin/${PKGNAME}

uninstall-client:
	$(MAKE) -C client/ uninstall

clean: | clean-cache clean-server clean-client

clean-cache:
	+go clean -cache -modcache -testcache all

clean-server:
	+rm -f ${PKGNAME}

clean-client:
	+$(MAKE) -C client/ clean
