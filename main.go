package main;

import (
    "context"
    "gitlab.com/mrvik/logger"
    "gitlab.com/mrvik/logger/log"
    "gitlab.com/mrvik/smb-web-client/sambaManager"
    "gitlab.com/mrvik/smb-web-client/server"
    "gitlab.com/mrvik/smb-web-client/signals"
    "gitlab.com/mrvik/smb-web-client/types"
    "os"
)

var lgr logger.Logger;

func main(){
    lgr=log.CreateDebug("smb-web-client", nil, nil);
    defer recoverAndDie();
    config:=types.ConfigFromFlags();
    lgr.Info("Starting SMB Web client");
    ctx, cancel:=context.WithCancel(context.Background());
    signals.Init(ctx, cancel);
    requestChannel:=server.Init(ctx, config, lgr);
    err:=sambaManager.Init(ctx, lgr, config, requestChannel);
    if err != nil{
        panic(err);
    }
    <-ctx.Done();
    lgr.Info("Exiting");
}

func recoverAndDie(){
    if err:=recover(); err != nil && lgr != nil {
        lgr.Errorf("Panic on main: %s\n", err);
        os.Exit(1);
    }else if err != nil {
        panic(err); //Panic if logger wasn't initialized
    }
}
