package main;

import(
    "gitlab.com/mrvik/logger"
    lgr "gitlab.com/mrvik/logger/log"
    "gitlab.com/mrvik/smb-web-client/cryptUtil"
    "syscall/js"
    "time"
)

var (
    log logger.Logger;
    crypt *cryptUtil.Crypt;
    registerFns=map[string]func(js.Value, []js.Value)interface{}{
        "initCrypt": createCrypt,
        "encryptData": encrypt,
        "logError": logError,
    };
    exportedFns=make(map[string]interface{});
    exports=map[string]interface{}{
        "functions": exportedFns,
    };
)

func main(){
    log=lgr.Create("go", nil, nil);
    log.Debug("Registering functions");
    for name,fn:=range registerFns{
        fun:=js.FuncOf(fn);
        exportedFns[name]=fun;
    }
    js.Global().Set("goExports", exports);
    log.Debug("Functions registered OK!");
    select{}; //Lock forever
}

func createCrypt(_ js.Value, args []js.Value) interface{}{
    var pubKeyB64 string;
    if len(args) > 0{
        if v:=args[0]; v.Type() != js.TypeString{
            return createJSError("First argument must be string");
        }else{
            pubKeyB64=v.String();
        }
    }else{
        return createJSError("Wrong number of parameters");
    }
    var err error;
    crypt, err=cryptUtil.InitPublic(log, pubKeyB64);
    if err != nil{
        return err;
    }
    log.Debug("Crypt is ready");
    return js.Undefined();
}

func encrypt(_ js.Value, args []js.Value) interface{}{
    if len(args) < 1{
        return createJSError("Wrong number of parameters");
    }
    if crypt == nil{
        return createJSError("Crypt is not initialized");
    }
    start:=time.Now();
    var plaintext string;
    if v:=args[0]; v.Type() != js.TypeString{
        return createJSError("First parameter must be string");
    }else{
        plaintext=v.String();
    }
    b64c, err:=crypt.Encrypt([]byte(plaintext));
    if err != nil{
        return err;
    }
    end:=time.Now();
    log.Debugf("Encrypted %d bytes in %.2f seconds\n", len(plaintext), end.Sub(start).Seconds());
    return b64c;
}

func logError(_ js.Value, args []js.Value) interface{}{
    for _,v:=range args{
        log.Error(v.String());
    }
    return js.Undefined();
}

func createJSError(err string) js.Value{
    return js.Global().Get("Error").New(err);
}
