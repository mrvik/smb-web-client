/* global goImportWait goExports lang */
const API_ENDPOINT="/api/";

const API_REQ_GET_KEY=0;
const API_REQ_CHANGE_PASSWORD=1;

var logItem;


//Send an API request to endpoint. Type should be from API_REQ_* consts
function sendRequest(type, data){
    const apiData={
        Type: type,
        ...data,
    }
    const request=new Request(API_ENDPOINT, {
        method: 'POST',
        mode: 'same-origin',
        body: JSON.stringify(apiData),
    });
    return fetch(request)
        .then(response=>response.json())
        .catch(e=>{
            console.error(e);
            throw e;
        });
}

async function getPubkey(){
    let rsp=await sendRequest(API_REQ_GET_KEY);
    if(rsp.HasError) throw new Error(rsp.Error||rsp.Message);
    else if(rsp.Message=="")throw new Error("Got empty message from server");
    console.debug("Waiting for Go Imports");
    await goImportWait;
    let err=goExports.functions.initCrypt(rsp.Message);
    if(err instanceof Error){
        throw err;
    }
}

//Load the indicated language
function _loadLang(lng){
    return fetch(`lang/${lng}.json`)
        .then(response=>{
            if(!response.ok) throw new Error(`response has status ${response.status}`);
            return response.json();
        })
        .then(obj=>self.lang=obj)
        .then(()=>console.log(self.lang.EventRegistry));
}

async function loadLang(){
    try{
        await _loadLang(navigator.language);
    }catch(e){
        log(`Language ${navigator.language} not available on server. Using en`);
        await _loadLang("en");
    }
}

async function start(){
    logItem=document.querySelector("pre#log");
    console.log=log;
    goImportWait.then(()=>console.debug("Go Imports ready"));
    await loadLang();
    await getPubkey();
    createEvents();
}

function main(){
    start().catch(e=>{
        log(`Error: ${e}`);
    });
}
self.addEventListener("load", main);

function createEvents(){
    document.querySelector("form>button:last-of-type").addEventListener("click", event=>{
        let username=document.getElementById("user");
        let curPass=document.getElementById("pass");
        let newPass=document.getElementById("newPassword");
        let newPassR=document.getElementById("repeatNewPassword");
        if(newPass.value!==newPassR.value){
            goExports.functions.logError(lang.PasswordsNotMatch);
            return;
        }
        if(anyIs("", username.value, curPass.value, newPass.value)){
            goExports.functions.logError(lang.EmptyField);
            return;
        }
        event.target.disabled=true;
        changePassword(username.value, curPass.value, newPass.value).then(()=>{
            event.target.disabled=false;
        })
    })
}

async function changePassword(user, password, newPassword){
    let data={
        Username: user,
        Password: goExports.functions.encryptData(password),
        NewPassword: goExports.functions.encryptData(newPassword),
    };
    console.log(lang.ChangePasswordOngoing);
    try{
        let response=await sendRequest(API_REQ_CHANGE_PASSWORD, data);
        if(!response || response.HasError){
            throw new Error(response.Message);
        }else{
            console.log(response.Message);
            document.querySelectorAll("form input").forEach(input=>input.value="");
            document.querySelector("form input:first-of-type").focus();
        }
    }catch(ex){
        goExports.functions.logError(`${lang.ChangePasswordError}: ${ex.message||ex}`);
        console.error(ex);
    }
}

function anyIs(model, ...strings){
    return strings.some(f=>f==model);
}

function log(thing){
    if(typeof thing != "object")logItem.innerText+=thing+"\n";
    console.info(thing);
}
