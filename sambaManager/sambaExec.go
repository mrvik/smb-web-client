package sambaManager;

import(
    "bytes"
    "context"
    "fmt"
    "gitlab.com/mrvik/logger"
    "gitlab.com/mrvik/smb-web-client/types"
    "gitlab.com/mrvik/smb-web-client/utils"
    "io/ioutil"
    "os/exec"
    "strings"
    "time"
)

const(
    EXEC_TIMEOUT=time.Second*4;
)

var (
    smbpasswdCommand string;
    log logger.Logger;
)

//Possible errors
var(
    ErrSambaMissing=fmt.Errorf("samba utilities are missing on server");
)

func InitExec(lgr logger.Logger) (err error){
    log=lgr;
    smbpasswdCommand, err=exec.LookPath("smbpasswd");
    return;
}

func ChangePassword(ctx context.Context, host, user, oldPassword, newPassword string) (err error){
    if smbpasswdCommand == "" {
        panic(ErrSambaMissing); //Can't continue w/o the samba utilities
    }
    if utils.AnyStringIs("", host, user, oldPassword, newPassword) {
        return fmt.Errorf("bad request");
    }
    cmd:=exec.CommandContext(ctx, smbpasswdCommand, generateSmbpasswdArguments(host, user)...);
    stdinString:=utils.JoinStrings("\n", oldPassword, newPassword, newPassword)+"\n";
    cmd.Stdin=bytes.NewBufferString(stdinString);
    stdout, err:=cmd.StdoutPipe();
    if err != nil{
        return;
    }
    stderr, err:=cmd.StderrPipe();
    if err != nil{
        return;
    }
    err=cmd.Start();
    if err != nil{
        return;
    }
    readStdout,_:=ioutil.ReadAll(stdout);
    readStderr,_:=ioutil.ReadAll(stderr);
    err=cmd.Wait();
    if err != nil {
        strErr:=string(readStderr);
        if strings.Contains(strErr, "NT_STATUS_LOGON_FAILURE"){
            err=types.ErrBadPassword;
        }else if strings.Contains(strErr, "password update rule has been violated"){
            err=types.ErrWeakPassword;
        }else if strings.Contains(strErr, "NT_STATUS_UNSUCCESSFUL"){
            err=types.ErrServerFailure;
        }else{
            err=fmt.Errorf("internal error: %s", strErr);
            log.Warnf("Stdout from %s:\n%s", smbpasswdCommand, readStdout);
            log.Warnf("Stderr from %s:\n%s", smbpasswdCommand, readStderr);
        }
    }
    return;
}

func generateSmbpasswdArguments(host, user string)(arguments []string){
    arguments=[]string{
        "-s", //Read from stdin
        "-r", host,
        "-U", user,
    };
    return;
}
