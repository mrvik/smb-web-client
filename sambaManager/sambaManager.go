package sambaManager;

import (
    "context"
    "fmt"
    "gitlab.com/mrvik/logger"
    "gitlab.com/mrvik/smb-web-client/types"
    "gitlab.com/mrvik/smb-web-client/cryptUtil"
    "gitlab.com/mrvik/smb-web-client/utils"
)

var (
    crypt *cryptUtil.Crypt;
    config *types.Config;
);

func Init(ctx context.Context, lgr logger.Logger, cfg *types.Config, reqChannel <-chan *types.SambaRequest) error{
    config=cfg;
    var err error;
    crypt, err=cryptUtil.InitCrypt(lgr);
    if err != nil{
        return err;
    }
    err=InitExec(lgr);
    if err != nil{
        return err;
    }
    go func(){
        listenLoop:
        for{
            select{
            case <-ctx.Done():
                break listenLoop;
            case request:=<-reqChannel:
                lgr.Debugf("Got a request type %d: %+v\n", request.RequestType, *request.RequestData);
                lgr.Infof("Got a request type %d\n", request.RequestType);
                go handleRequest(request);
            }
        }
    }();
    return nil;
}

func handleRequest(request *types.SambaRequest){
    var decPassword, decNewPassword string;
    data:=request.RequestData;
    var err error;
    if data.Password != "" { //Decrypt password if needed
        decPassword, err=crypt.Decrypt(data.Password);
    }
    if err != nil{
        request.ResponseChannel<-&types.Response{
            Ok: false,
            Status: 400,
            Error: err,
        };
        return;
    }
    if data.NewPassword != "" {
        decNewPassword, err=crypt.Decrypt(data.NewPassword);
    }
    if err != nil{
        request.ResponseChannel<-&types.Response{
            Ok: false,
            Status: 400,
            Error: err,
        };
        return;
    }
    response:=&types.Response{};
    switch data.Type{
    case 0: //Get public key
        response.ResponseData=crypt.GetPubkey();
        response.Ok=response.ResponseData!="";
    case 1: //Change password
        if utils.AnyStringIs("", data.Username, decPassword, decNewPassword){
            response.Ok=false;
            response.Error=fmt.Errorf("Bad request");
            response.Status=400;
            break;
        }
        response.Error=ChangePassword(request.Context, config.SambaHost, data.Username, decPassword, decNewPassword);
        if response.Error != nil{
            switch response.Error{
            case types.ErrServerFailure:
                response.Status=503; //Service unavailable
            default:
                response.Status=401; //Unauthorized
            }
        }else{
            response.ResponseData=types.LanguageMap["ChangePasswordOK"];
        }
    default:
        response.Status=400;
        response.Error=types.ErrBadRequest;
    }
    request.ResponseChannel<-response;
}
