module gitlab.com/mrvik/smb-web-client

go 1.13

require (
	github.com/mvo5/libsmbclient-go v0.0.0-20190424080658-d00e6e47f104
	gitlab.com/mrvik/logger v0.0.0-20191229144008-335e05dc4e45
)
