package cryptUtil;

import(
    "crypto/rand"
    "crypto/rsa"
    "crypto/x509"
    "encoding/base64"
    "errors"
    "gitlab.com/mrvik/logger"
)

var (
    ErrNoPrivKey=errors.New("private key not present on Crypt");
)

type Crypt struct{
    privateKey *rsa.PrivateKey;
    publicKey *rsa.PublicKey;
}

//Create a key pair and assoc the logger
func InitCrypt(log logger.Logger) (*Crypt, error){
    log.Info("Generating key...");
    generatedKey, err:=rsa.GenerateKey(rand.Reader, 2048);
    if err!=nil{
        return nil, err;
    }
    log.Info("Key generated OK!");
    crypt:=&Crypt{
        privateKey: generatedKey,
        publicKey: &generatedKey.PublicKey,
    }
    return crypt,nil;
}

//Create a Crypt from a public key (only with encrypt ability) and assoc the logger
func InitPublic(log logger.Logger, pubKeyB64 string) (*Crypt, error){
    key, err:=base64.StdEncoding.DecodeString(pubKeyB64);
    if err != nil{
        return nil, err;
    }
    pkey, err:=x509.ParsePKCS1PublicKey(key);
    if err != nil{
        return nil, err;
    }
    crypt:=&Crypt{
        publicKey: pkey,
    }
    return crypt, nil;
}

//Return pubkey as base64 string
func (cr *Crypt) GetPubkey() string{
    return base64.StdEncoding.EncodeToString(x509.MarshalPKCS1PublicKey(cr.publicKey));
}

//Parse a bas64-encoded string and decrypt data. Then return it as a string
func (cr *Crypt) Decrypt(base64String string) (decrypted string, err error){
    if cr.privateKey == nil{
        return "", ErrNoPrivKey;
    }
    bytes, err:=base64.StdEncoding.DecodeString(base64String);
    if err != nil {
        return;
    }
    text, err:=cr.privateKey.Decrypt(rand.Reader, bytes, nil);
    if err != nil{
        return;
    }
    decrypted=string(text);
    return;
}

//Encrypt a byte slice and output it encoded as base64
func (cr *Crypt) Encrypt(data []byte) (cb64 string, err error){
    ciphertext, err:=rsa.EncryptPKCS1v15(rand.Reader, cr.publicKey, data);
    if err != nil{
        return;
    }
    return base64.StdEncoding.EncodeToString(ciphertext), nil;
}
